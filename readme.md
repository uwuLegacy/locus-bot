# LocusBOT

Discord bot for locus.solutions.

## Some information

We are currently using ts-node, the tsc compiler just checks for errors without creating any files.

## Installation

Please do yourself a favour and use pnpm to install packages.

You may need to regenerate the Prisma client (it should tell you if you do), to do that run `pnpm prisma:gen`.

Fill out the [.env.example](.env.example) file and rename it to just .env.

## Contributing

See [our contribution guide](contributing.md).

## Credits

Developed by Legacy for Locus.solutions

## License

Unlicensed as of now
